#import "CDVDigits.h"
#import <Fabric/Fabric.h>
#import <DigitsKit/DigitsKit.h>
#import <Cordova/CDV.h>

@implementation CDVDigits

- (void)login:(CDVInvokedUrlCommand*)command
{

    /*
    [[Digits sharedInstance] authenticateWithCompletion:^
            (DGTSession* session, NSError *error) {
        
      CDVPluginResult* pluginResult = nil;

        if (session) {
            NSLog(@"session data is : %@", session);
            
          pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        } else {
          pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
    
      [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];        
    
    }];
    */
    
    Digits *digits = [Digits sharedInstance];
    [digits authenticateWithCompletion:^(DGTSession *session, NSError *error) {
        CDVPluginResult* pluginResult = nil;
        if (session) {
            // NSLog(@"session data is : %@", session);
            
            
            DGTOAuthSigning *oauthSigning = [[DGTOAuthSigning alloc] initWithAuthConfig:digits.authConfig authSession:digits.session];
            NSDictionary *authHeaders = [oauthSigning OAuthEchoHeadersToVerifyCredentials];
            // NSLog(@"auth header is : %@", authHeaders);
            
            //NSString *authToken = [authHeaders[@"X-Verify-Credentials-Authorization"] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            NSString *authToken = authHeaders[@"X-Verify-Credentials-Authorization"];
            // NSLog(@"auth token is : %@", authToken);
            
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:authToken];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];

}

@end
